package com.ae.order.service;

import com.ae.order.model.Offers;
import com.ae.order.model.OrderDetails;
import com.ae.order.model.OrderResponse;
import com.ae.order.util.Util;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataService {
    public List<Offers> getOfferList() {
        return Util.getOfferList();
    }
    private static final Map<String, OrderResponse> oderResponseData= new HashMap();

    public String generateOrderId() {
        return String.valueOf(oderResponseData.size()+1);
    }
    public void createOrders(OrderDetails orderDetails) {
        oderResponseData.put(orderDetails.getOrderId(),orderDetails.getOrderResponse());
    }

    public Map<String, OrderResponse> getAllOrders() {
        return oderResponseData;
    }

    public OrderResponse getOrderById(String id) {
        return oderResponseData.get(id);
    }
}
