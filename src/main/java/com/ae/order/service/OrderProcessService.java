package com.ae.order.service;

import com.ae.order.model.*;
import com.ae.order.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class OrderProcessService implements OrderProcess{

    @Autowired
    DataService dataService;

    @Autowired
    OrderDetails orderDetails;

    @Override
    public OrderResponse createOrder(List<OrderRequest> orderList) {
        final List<ItemDetails> itemDetailsList = new ArrayList<>();
        final OrderResponse orderResponse = new OrderResponse();
        orderList.stream()
                .filter(Objects::nonNull)
                .forEach(orderRequest -> {
                    if(Util.isValidItem(orderRequest.getItemName())) {
                        itemDetailsList.add(processOrder(orderRequest));
                    }
                });
        orderResponse.getItemDetailsList().addAll(itemDetailsList);
        orderResponse.setTotalPrice(Util.totalPrice(itemDetailsList));
        final String orderId = dataService.generateOrderId();
        orderResponse.setOrderId(orderId);
        orderDetails.getOrderRequestList().addAll(orderList);
        orderDetails.setOrderResponse(orderResponse);
        orderDetails.setOrderId(orderId);
        dataService.createOrders(orderDetails);
        return orderResponse;
    }

    @Override
    public List<OrderResponse> getAllOrders() {
        Map<String, OrderResponse> oderData = dataService.getAllOrders();
        return new ArrayList<>(oderData.values());
    }

    @Override
    public OrderResponse getOrderById(String id) {
        return dataService.getOrderById(id);
    }


    private ItemDetails processOrder(OrderRequest orderRequest)  {


        final List<Offers> offersList= dataService.getOfferList();

        if(offersList.isEmpty()) {
            return setItemDetails(orderRequest);
        }

        Optional<Offers> offer= offersList.stream()
                .filter(Objects::nonNull)
                .filter(offer1 -> offer1.getOfferItemName().equalsIgnoreCase(orderRequest.getItemName())).findAny();

        if (offer.isPresent() && offer.get().getOfferType().equalsIgnoreCase("Products")) {
            return getProductOffer(orderRequest, offer.get());
        } else if (offer.isPresent() && offer.get().getOfferType().equalsIgnoreCase("Price")) {
            return getPriceOffer(orderRequest, offer.get());
        } else {
            return setItemDetails(orderRequest);
        }

    }

    private ItemDetails setItemDetails(OrderRequest orderRequest) {
        ItemDetails itemDetails = new ItemDetails();
        itemDetails.setItem(orderRequest.getItemName());
        itemDetails.setQuantity(orderRequest.getQuanity());
        itemDetails.setPrice(Util.calculateItemCost(orderRequest.getItemName(), orderRequest.getQuanity()));
        return itemDetails;
    }

    private ItemDetails getProductOffer(OrderRequest orderRequest, Offers offers) {
        final ItemDetails itemDetails = new ItemDetails();
        itemDetails.setItem(orderRequest.getItemName());
        if (orderRequest.getItemName().equalsIgnoreCase(offers.getOfferItemName())) {
            if (orderRequest.getQuanity() >= offers.getMinimumNumberOfItem()) {
                itemDetails.setQuantity(Util.calculateNumberofOfferItem(orderRequest.getQuanity(),
                        offers.getMinimumNumberOfItem(), offers.getNumberOfOfferItem()));

            } else {
                itemDetails.setQuantity(orderRequest.getQuanity());
            }

        }else {
            itemDetails.setQuantity(orderRequest.getQuanity());
        }
        itemDetails.setPrice(Util.calculateItemCost(orderRequest.getItemName(), orderRequest.getQuanity()));
        return itemDetails;
    }

    private ItemDetails getPriceOffer(OrderRequest orderRequest, Offers offers) {
        final ItemDetails itemDetails = new ItemDetails();
        itemDetails.setItem(orderRequest.getItemName());
        itemDetails.setQuantity(orderRequest.getQuanity());
        if (orderRequest.getItemName().equalsIgnoreCase(offers.getOfferItemName())) {
            int orderquanity = orderRequest.getQuanity();
            int requiredQuanity = offers.getMinimumNumberOfItem();
            if (orderquanity >= requiredQuanity) {
                int eligibleOffer = orderquanity / requiredQuanity;
                int remainder = orderquanity % requiredQuanity;
                int eligibleOfferPricedQuntity = eligibleOffer * offers.getNumberOfOfferItem();
                int pricedQuantity = eligibleOfferPricedQuntity + remainder;
                itemDetails.setPrice(Util.calculateItemCost(orderRequest.getItemName(), pricedQuantity));
            } else {
                itemDetails.setPrice(Util.calculateItemCost(orderRequest.getItemName(), orderRequest.getQuanity()));
            }

        }  else {
            itemDetails.setPrice(Util.calculateItemCost(orderRequest.getItemName(), orderRequest.getQuanity()));
        }
        return itemDetails;
    }

}
