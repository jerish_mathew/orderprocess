package com.ae.order.service;

import com.ae.order.model.OrderRequest;
import com.ae.order.model.OrderResponse;

import java.util.List;

public interface OrderProcess {

    OrderResponse createOrder(List<OrderRequest> orderList);
    List<OrderResponse> getAllOrders();
    OrderResponse getOrderById(String id);
}
