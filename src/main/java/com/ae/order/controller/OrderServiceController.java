package com.ae.order.controller;

import com.ae.order.model.OrderRequest;
import com.ae.order.model.OrderResponse;
import com.ae.order.service.OrderProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orderservice")
public class OrderServiceController {

    @Autowired
    OrderProcess orderProcess;



    @PostMapping(value = "/createorder", consumes = MediaType.APPLICATION_JSON_VALUE)
    public OrderResponse createOrder(@RequestBody List<OrderRequest> orderList) {
        return orderProcess.createOrder(orderList);
    }

    @GetMapping(value = "/getallorders")
    public List<OrderResponse> getOrders() {
        return orderProcess.getAllOrders();
    }


    @GetMapping(value = "/getorderbyid/{id}")
    public OrderResponse getOrderById(@PathVariable String id) {
        return orderProcess.getOrderById(id);
    }


}


