package com.ae.order.util;

import com.ae.order.model.ItemDetails;
import com.ae.order.model.Offers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class Util {


    public static final Double getItemPrice(final String itemName) {
        //Hard coded price.
        if (Items().containsKey(itemName.toLowerCase())) {
            return (Double) Items().get(itemName.toLowerCase());
        } else {
            return null;
        }


    }

    private static final Map Items() {
        final HashMap<String, Double> itmMap = new HashMap<>();
        itmMap.put("orange", .25);
        itmMap.put("apple", .60);
        return itmMap;
    }

    public static final boolean isValidItem(String itemName) {
        return Items().containsKey(itemName.toLowerCase());
    }

    public static final Map<String, Offers> getOffers() {
        final HashMap<String, Offers> offers = new HashMap<>();
        offers.put("Product", getAppleOffers());
        offers.put("Price", getOrrangeOffers());
        return offers;
    }

    public static final List<Offers> getOfferList() {
        final List<Offers> offersList = new ArrayList<>();
        offersList.add(getAppleOffers());
        offersList.add(getOrrangeOffers());
        return offersList;
    }

    private static final Offers getAppleOffers() {
        Offers offers = new Offers();
        offers.setOfferType("Products");
        offers.setOfferDescription("buy one get one free");
        offers.setMinimumNumberOfItem(1);
        offers.setOfferItemName("apple");
        offers.setNumberOfOfferItem(1);
        return offers;
    }

    public static final Offers getOrrangeOffers() {
        Offers offers = new Offers();
        offers.setOfferType("Price");
        offers.setOfferDescription("3 for the price of 2");
        offers.setMinimumNumberOfItem(3);
        offers.setOfferItemName("orange");
        offers.setNumberOfOfferItem(2);
        return offers;
    }

    public static final int calculateNumberofOfferItem(int orderquanity, int requiredQuanity, int offerQuanitity) {
        int eligibleOfferNumber = orderquanity/requiredQuanity;
        int eligiblefferQuanity = eligibleOfferNumber * offerQuanitity;
        int totalQuanity = orderquanity + eligiblefferQuanity;
        return totalQuanity;
    }

    public static final Double calculateItemCost (final String itemName, final int quantity) {
        final double dquantity = Double.valueOf(quantity);
        Optional<Double> price = Optional.ofNullable(Util.getItemPrice(itemName));
        final double itemCost = Double.valueOf(quantity) * price.orElse(0.00);
        BigDecimal bd = new BigDecimal(Double.toString(itemCost));
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static final Double totalPrice(List<ItemDetails> itemDetailsList) {
        Double totalprice =itemDetailsList
                .stream()
                .map(item -> item.getPrice())
                .reduce(0.00,Double::sum);
        BigDecimal bd = new BigDecimal(Double.toString(totalprice));
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
