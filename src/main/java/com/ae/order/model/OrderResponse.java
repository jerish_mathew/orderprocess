package com.ae.order.model;

import java.util.ArrayList;
import java.util.List;

public class OrderResponse {


    private List<ItemDetails> itemDetailsList = new ArrayList<>();
    private Double totalPrice;
    private String orderId;

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<ItemDetails> getItemDetailsList() {
        return itemDetailsList;
    }

}
