package com.ae.order.model;

public class Offers {

    private String offerType;
    private String offerDescription;
    private String offerItemName;
    private int minimumNumberOfItem;
    private int numberOfOfferItem;

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public String getOfferItemName() {
        return offerItemName;
    }

    public void setOfferItemName(String offerItemName) {
        this.offerItemName = offerItemName;
    }

    public int getMinimumNumberOfItem() {
        return minimumNumberOfItem;
    }

    public void setMinimumNumberOfItem(int minimumNumberOfItem) {
        this.minimumNumberOfItem = minimumNumberOfItem;
    }

    public int getNumberOfOfferItem() {
        return numberOfOfferItem;
    }

    public void setNumberOfOfferItem(int numberOfOfferItem) {
        this.numberOfOfferItem = numberOfOfferItem;
    }

}

