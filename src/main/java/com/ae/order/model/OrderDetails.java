package com.ae.order.model;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OrderDetails {

    private List<OrderRequest> orderRequestList = new ArrayList<>();
    private OrderResponse orderResponse;
    private String orderId;

    public List<OrderRequest> getOrderRequestList() {
        return orderRequestList;
    }

    public OrderResponse getOrderResponse() {
        return orderResponse;
    }

    public void setOrderResponse(OrderResponse orderResponse) {
        this.orderResponse = orderResponse;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }


}
