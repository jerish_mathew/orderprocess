package com.ae.order;

import com.ae.order.model.*;
import com.ae.order.service.DataService;
import com.ae.order.service.OrderProcessService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    @InjectMocks
    OrderProcessService orderProcessService = new OrderProcessService();

    @Mock
    OrderDetails orderDetails = new OrderDetails();

    @Mock
    DataService dataService;

    @Test
    public void createOrdr_Single_Item_Test() {
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setItemName("apple");
        orderRequest.setQuanity(2);
        List<OrderRequest> orderList = new ArrayList<>();
        orderList.add(orderRequest);
        OrderResponse orderResponse = orderProcessService.createOrder(orderList);
        assert(orderResponse!=null);
        assert (orderResponse.getItemDetailsList().size()==1);
        assert (orderResponse.getItemDetailsList().get(0).getPrice()==1.2);
        assert (orderResponse.getItemDetailsList().get(0).getItem().equalsIgnoreCase("apple"));
        assert (orderResponse.getItemDetailsList().get(0).getQuantity()==2);

        assert (orderResponse.getTotalPrice() == 1.2);
    }

    @Test
    public void createOrdr_MoreThanOneItem_Test() {
        List<OrderRequest> orderList = new ArrayList<>();
        OrderRequest orderRequest1 = new OrderRequest();
        orderRequest1.setItemName("apple");
        orderRequest1.setQuanity(2);

        OrderRequest orderRequest2 = new OrderRequest();
        orderRequest2.setItemName("Orange");
        orderRequest2.setQuanity(3);

        orderList.add(orderRequest1);
        orderList.add(orderRequest2);

        OrderResponse orderResponse = orderProcessService.createOrder(orderList);
        assert(orderResponse!=null);
        assert (orderResponse.getItemDetailsList().size()==2);
        assert (orderResponse.getItemDetailsList().get(0).getPrice()==1.2);
        assert (orderResponse.getItemDetailsList().get(0).getItem().equalsIgnoreCase("apple"));
        assert (orderResponse.getItemDetailsList().get(0).getQuantity()==2);

        assert (orderResponse.getItemDetailsList().get(1).getPrice()==0.75);
        assert (orderResponse.getItemDetailsList().get(1).getItem().equalsIgnoreCase("orange"));
        assert (orderResponse.getItemDetailsList().get(1).getQuantity()==3);

        assert (orderResponse.getTotalPrice() == 1.95);
    }

    @Test
    public void createOrdr_CaseSensitive_Test() {
        List<OrderRequest> orderList = new ArrayList<>();
        OrderRequest orderRequest1 = new OrderRequest();
        orderRequest1.setItemName("APPLE");
        orderRequest1.setQuanity(2);

        OrderRequest orderRequest2 = new OrderRequest();
        orderRequest2.setItemName("ORANGE");
        orderRequest2.setQuanity(3);

        orderList.add(orderRequest1);
        orderList.add(orderRequest2);

        OrderResponse orderResponse = orderProcessService.createOrder(orderList);
        assert(orderResponse!=null);
        assert (orderResponse.getItemDetailsList().size()==2);
        assert (orderResponse.getItemDetailsList().get(0).getPrice()==1.2);
        assert (orderResponse.getItemDetailsList().get(0).getItem().equalsIgnoreCase("apple"));
        assert (orderResponse.getItemDetailsList().get(0).getQuantity()==2);

        assert (orderResponse.getItemDetailsList().get(1).getPrice()==0.75);
        assert (orderResponse.getItemDetailsList().get(1).getItem().equalsIgnoreCase("orange"));
        assert (orderResponse.getItemDetailsList().get(1).getQuantity()==3);
        assert (orderResponse.getTotalPrice() == 1.95);
    }

    @Test
    public void createOrdr_WithNotAvailableItem_Test() {
        List<OrderRequest> orderList = new ArrayList<>();
        OrderRequest orderRequest1 = new OrderRequest();
        orderRequest1.setItemName("Apple");
        orderRequest1.setQuanity(2);

        OrderRequest orderRequest2 = new OrderRequest();
        orderRequest2.setItemName("Mango"); //The Mango data not available, this should not show in response
        orderRequest2.setQuanity(3);

        orderList.add(orderRequest1);
        orderList.add(orderRequest2);

        OrderResponse orderResponse = orderProcessService.createOrder(orderList);
        assert(orderResponse!=null);
        assert (orderResponse.getItemDetailsList().size()==1);
        assert (orderResponse.getItemDetailsList().get(0).getPrice()==1.2);
        assert (orderResponse.getItemDetailsList().get(0).getItem().equalsIgnoreCase("apple"));
        assert (orderResponse.getItemDetailsList().get(0).getQuantity()==2);

        assert (orderResponse.getTotalPrice() == 1.2);
    }

    @Test
    public void offerOnItemAndPrice_Test() {
        //Testing both offer on item and price.
        //In Response quantity of apple should show based on offer
        //And price of orange and total price should show based on offer

        List<OrderRequest> orderList = new ArrayList<>();
        OrderRequest orderRequest1 = new OrderRequest();
        orderRequest1.setItemName("Apple");
        orderRequest1.setQuanity(2);

        OrderRequest orderRequest2 = new OrderRequest();
        orderRequest2.setItemName("Orange");
        orderRequest2.setQuanity(7);

        orderList.add(orderRequest1);
        orderList.add(orderRequest2);


        when(dataService.getOfferList()).thenReturn(getOfferList());
        OrderResponse orderResponse = orderProcessService.createOrder(orderList);
        assert(orderResponse!=null);
        assert (orderResponse.getItemDetailsList().size()==2);
        assert (orderResponse.getItemDetailsList().get(0).getPrice()==1.2);
        assert (orderResponse.getItemDetailsList().get(0).getItem().equalsIgnoreCase("apple"));
        assert (orderResponse.getItemDetailsList().get(0).getQuantity()==4);

        assert (orderResponse.getItemDetailsList().get(1).getPrice()==1.25);
        assert (orderResponse.getItemDetailsList().get(1).getItem().equalsIgnoreCase("orange"));
        assert (orderResponse.getItemDetailsList().get(1).getQuantity()==7);
        assert (orderResponse.getTotalPrice() == 2.45);

    }

    @Test
    public void offerOnItemTest() {
        //Enabled offer for Item only.
        List<OrderRequest> orderList = new ArrayList<>();
        OrderRequest orderRequest1 = new OrderRequest();
        orderRequest1.setItemName("Apple");
        orderRequest1.setQuanity(2);

        OrderRequest orderRequest2 = new OrderRequest();
        orderRequest2.setItemName("Orange");
        orderRequest2.setQuanity(7);

        orderList.add(orderRequest1);
        orderList.add(orderRequest2);

        final List<Offers> offersList = new ArrayList<>();
        offersList.add(getAppleOffers());
        //offersList.add(getOrrangeOffers());

        when(dataService.getOfferList()).thenReturn(offersList);
        OrderResponse orderResponse = orderProcessService.createOrder(orderList);
        assert(orderResponse!=null);
        assert (orderResponse.getItemDetailsList().size()==2);
        assert (orderResponse.getItemDetailsList().get(0).getPrice()==1.2);
        assert (orderResponse.getItemDetailsList().get(0).getItem().equalsIgnoreCase("apple"));
        assert (orderResponse.getItemDetailsList().get(0).getQuantity()==4);

        assert (orderResponse.getItemDetailsList().get(1).getPrice()==1.75);
        assert (orderResponse.getItemDetailsList().get(1).getItem().equalsIgnoreCase("orange"));
        assert (orderResponse.getItemDetailsList().get(1).getQuantity()==7);
        assert (orderResponse.getTotalPrice() == 2.95);

    }

    @Test
    public void offerTestWithPrice() {
        //Enabled offer for price only.
        List<OrderRequest> orderList = new ArrayList<>();
        OrderRequest orderRequest1 = new OrderRequest();
        orderRequest1.setItemName("Apple");
        orderRequest1.setQuanity(2);

        OrderRequest orderRequest2 = new OrderRequest();
        orderRequest2.setItemName("Orange");
        orderRequest2.setQuanity(3);

        orderList.add(orderRequest1);
        orderList.add(orderRequest2);

        final List<Offers> offersList = new ArrayList<>();
        //offersList.add(getAppleOffers());
        offersList.add(getOrrangeOffers());

        when(dataService.getOfferList()).thenReturn(offersList);
        OrderResponse orderResponse = orderProcessService.createOrder(orderList);
        assert(orderResponse!=null);
        assert (orderResponse.getItemDetailsList().size()==2);
        assert (orderResponse.getItemDetailsList().get(0).getPrice()==1.2);
        assert (orderResponse.getItemDetailsList().get(0).getItem().equalsIgnoreCase("apple"));
        assert (orderResponse.getItemDetailsList().get(0).getQuantity()==2);

        assert (orderResponse.getItemDetailsList().get(1).getPrice()==0.50);
        assert (orderResponse.getItemDetailsList().get(1).getItem().equalsIgnoreCase("orange"));
        assert (orderResponse.getItemDetailsList().get(1).getQuantity()==3);
        assert (orderResponse.getTotalPrice() == 1.7);

    }

    @Test
    public void getAllOrdersTest() {
        when(dataService.getAllOrders()).thenReturn(orders());
        List<OrderResponse> orderResponseList = orderProcessService.getAllOrders();
        assert (orderResponseList.size()==2);
    }

    @Test
    public void getOrderByIdTest() {
        when(dataService.getOrderById("order1")).thenReturn(orderResponse());
        OrderResponse orderResponse = orderProcessService.getOrderById("order1");
        assert (orderResponse.getOrderId().equalsIgnoreCase("order1"));
        assert (orderResponse.getItemDetailsList().size()==2);
    }

    private List<Offers> getOfferList() {
        final List<Offers> offersList = new ArrayList<>();
        offersList.add(getAppleOffers());
        offersList.add(getOrrangeOffers());
        return offersList;
    }

    private Offers getAppleOffers() {

        Offers offers = new Offers();
        offers.setOfferType("Products");
        offers.setOfferDescription("buy one get one free on Apples");
        offers.setMinimumNumberOfItem(1);
        offers.setOfferItemName("apple");
        offers.setNumberOfOfferItem(1);
        return offers;
    }

    private  Offers getOrrangeOffers() {

        Offers offers = new Offers();
        offers.setOfferType("Price");
        offers.setOfferDescription("3 for the price of 2 on Oranges");
        offers.setMinimumNumberOfItem(3);
        offers.setOfferItemName("orange");
        offers.setNumberOfOfferItem(2);
        return offers;
    }

    private Map<String, OrderResponse> orders() {

        final Map<String, OrderResponse> oderResponseData = new HashMap();

        oderResponseData.put("order1", orderResponse());

        OrderResponse orderResponse2 = new OrderResponse();
        ItemDetails itemDetails3 = new ItemDetails();
        itemDetails3.setItem("Apple");
        itemDetails3.setQuantity(2);
        itemDetails3.setPrice(.50);


        orderResponse2.getItemDetailsList().add(itemDetails3);

        orderResponse2.setOrderId("order2");
        orderResponse2.setTotalPrice(1.00);
        oderResponseData.put("order2", orderResponse2);
        return oderResponseData;
    }

    private OrderResponse orderResponse() {
        OrderResponse orderResponse = new OrderResponse();
        ItemDetails itemDetails1 = new ItemDetails();
        itemDetails1.setItem("Grapes");
        itemDetails1.setQuantity(3);
        itemDetails1.setPrice(1.00);

        ItemDetails itemDetails2 = new ItemDetails();
        itemDetails2.setItem("Mango");
        itemDetails2.setQuantity(2);
        itemDetails2.setPrice(2.00);

        orderResponse.getItemDetailsList().add(itemDetails1);
        orderResponse.getItemDetailsList().add(itemDetails2);
        orderResponse.setOrderId("order1");
        orderResponse.setTotalPrice(7.00);
        return orderResponse;
    }

}
